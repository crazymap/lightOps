package com.gsafety.devops.service;
import com.gsafety.devops.entity.SysConfigEntity;
import org.jeecgframework.core.common.service.CommonService;

import java.io.Serializable;

public interface SysConfigServiceI extends CommonService{
	
 	public void delete(SysConfigEntity entity) throws Exception;
 	
 	public Serializable save(SysConfigEntity entity) throws Exception;
 	
 	public void saveOrUpdate(SysConfigEntity entity) throws Exception;
 	
}
